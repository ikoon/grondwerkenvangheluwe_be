/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.mobileMenu = {
    attach: function (context, settings) {
      $(document).ready(function(){
        $('.menu-toggle').on('click',function(){
          $('.mobile-menu-wrapper').toggleClass('menu-open');
        });
      });
    }
  };

  /*Drupal.behaviors.specificClass = {
    attach: function (context, settings) {
      $(document).ready(function(){
        if($('.paragraph').hasClass('paragraph--type--image-text')){
          $('body').addClass('page-display-grid');
        }
      });
    }
  };*/

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.sliders = {
        attach: function (context, settings) {
            $(context).find('.swiper-container.field-name-field-images').once('ifHomeSlider').each(function (){
                $(document).ready(function () {
                    //initialize swiper when document ready
                    var mySwiper = new Swiper ('.swiper-container.field-name-field-images', {
                        // Optional parameters
                        loop: true,
                        effect: 'fade',
                        pagination: {
                            el: '.swiper-pagination',
                            clickable: true,
                        },
                    })
                });
            });
        }
    };

})(jQuery, Drupal);
